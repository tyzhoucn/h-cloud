package com.hcloud.audit.api.event;

import org.springframework.context.ApplicationEvent;

/**
 * 系统操作日志的
 * @Auther hepangui
 * @Date 2018/11/20
 */
public class OperateLogEvent extends ApplicationEvent {
    public OperateLogEvent(Object source) {
        super(source);
    }
}
