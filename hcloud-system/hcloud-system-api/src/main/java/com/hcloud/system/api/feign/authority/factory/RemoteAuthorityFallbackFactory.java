package com.hcloud.system.api.feign.authority.factory;

import com.hcloud.system.api.feign.authority.fallback.RemoteAuthorityFallbackImpl;
import feign.hystrix.FallbackFactory;
import com.hcloud.system.api.feign.authority.RemoteAuthorityService;
import org.springframework.stereotype.Component;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@Component
public class RemoteAuthorityFallbackFactory implements FallbackFactory<RemoteAuthorityService> {
    @Override
    public RemoteAuthorityService create(Throwable throwable) {
        return new RemoteAuthorityFallbackImpl(throwable);
    }
}
