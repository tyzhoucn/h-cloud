package com.hcloud.system.api.feign.authority;

import com.hcloud.system.api.feign.authority.factory.RemoteAuthorityFallbackFactory;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.ServerNameContants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@FeignClient(value = ServerNameContants.SYSTEM,fallbackFactory = RemoteAuthorityFallbackFactory.class)
public interface RemoteAuthorityService {
    @PostMapping (value = "/role/findAuthorityByRoleIds")
    HCloudResult<List<String>> findByRoleIds(@RequestParam("roleIds") String roleIds);
}
