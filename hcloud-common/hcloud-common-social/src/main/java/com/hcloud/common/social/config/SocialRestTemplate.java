package com.hcloud.common.social.config;

import org.springframework.web.client.RestTemplate;

/**
 * 处理AutoWird产生的No instances available for api.weixin.qq.com
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class SocialRestTemplate {
    private static RestTemplate restTemplate;
    public static RestTemplate getRestTemplate(){
        if(restTemplate == null){
            restTemplate = new RestTemplate();
        }
        return restTemplate;
    }
}
