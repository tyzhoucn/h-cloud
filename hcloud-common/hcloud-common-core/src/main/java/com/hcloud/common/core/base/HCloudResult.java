package com.hcloud.common.core.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 统一返回实体封装
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
public class HCloudResult<T> implements Serializable {
    public static final int NEEDLOGIN = -1;
    public static final int NOAUTH = -2;
    public static final int SUCCESS = 1;
    public static final int FAIL = 0;
    private static final long serialVersionUID = 1L;

    private String msg = "success";

    private int code = SUCCESS;

    private Long count;

    private T data;

    public HCloudResult() {

    }

    public HCloudResult(T data) {
        super();
        this.data = data;
    }

    public HCloudResult(T data, Long count) {
        super();
        this.data = data;
        this.count = count;
    }

    public HCloudResult(T data, Integer count) {
        super();
        this.data = data;
        this.count = 1L * count;
    }

    public HCloudResult(String msg) {
        super();
        this.code = FAIL;
        this.msg = msg;
    }

    public HCloudResult(Throwable e) {
        super();
        this.msg = e.getMessage();
        this.code = FAIL;
    }
}
